﻿using System;

namespace EX_HoanDoiHaiSo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Nhập hai số a và b để hoán đổi: ");

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine($"Trước hoán đổi: a = {a}, b = {b}");

            a = a + b;
            b = a - b;
            a = a - b;

            Console.WriteLine($"Sau hoán đổi: a = {a}, b = {b}");
        }
    }
}
